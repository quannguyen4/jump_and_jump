using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DigitalRuby.Tween;

public class PlatformMechanic : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        _UpdateInactive();
    }

    public void TweenIn()
    {
        System.Action<ITween<float>> DropPlatform = (t) =>
        {
            // start rotation from identity to ensure no stuttering
            transform.position = new Vector3(transform.position.x, t.CurrentValue, transform.position.z);
        };

        float startPos = transform.position.y;
        float endPos = startPos + 0.5f;

        // completion defaults to null if not passed in
        gameObject.Tween("DropDown", startPos, endPos, 0.5f, TweenScaleFunctions.QuadraticEaseOut, DropPlatform);
    }

    private bool _isOnView()
    {
        Vector3 screenPoint = Camera.main.WorldToViewportPoint(transform.position);
        bool onScreen = screenPoint.z > -0.5 && screenPoint.x > -1 && screenPoint.x < 2 && screenPoint.y > -1 && screenPoint.y < 2;
        // bool onScreen = screenPoint.z > -1;
        return onScreen;
    }

    private void _UpdateInactive()
    {
        if (!_isOnView())
        {
            gameObject.SetActive(false);
        }
    }
}
