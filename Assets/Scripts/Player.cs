using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using DigitalRuby.Tween;
using UnityEngine.UI;

public class Player : MonoBehaviour
{
    [SerializeField] private float jumpUp = 2;
    [SerializeField] private float jumpForward = 2;
    private Text scoreText;
    private bool canJump;
    private Rigidbody selfRigidbody;
    private bool isHolding = false;
    private float holdTime;
    private BoxCollider colliderSuccess;
    private bool isFirst = false;
    private bool isCollide = false;
    private bool isJumping = false;
    private float jumpAngle = 0;
    private float startScaleY;
    private bool isDied = false;
    private GameObject nextPlat;
    private int score;
    
    void Start(){
        selfRigidbody = GetComponent<Rigidbody>();
        colliderSuccess = GetComponent<BoxCollider>();
        jumpAngle = Mathf.PI/4;
        EventManager.onPlatformSpawned.AddListener(_SetNextPlatform);
        startScaleY = transform.localScale.y;
        TweenFactory.ClearTweensOnLevelLoad = true;
        isDied = false;
        holdTime = 1.0f;
        nextPlat = GameObject.FindWithTag("NextPlat");
        score = 0;
        scoreText = GameObject.FindWithTag("ScoreText").GetComponent<Text>();
    }
    
    void FixedUpdate(){
        if (isDied || !isFirst)
            return;
        if(canJump && !isHolding){
            _Jump();
        }
    }
    
    void Update(){
        if (isDied || !isFirst)
        {
            if (Input.GetMouseButtonDown(0))
            {
                SceneManager.LoadScene("MainScene");
            }
            return;
        }
        _OnButtonDown();
        _OnButtonUp();
        _OnButtonHHold();
    }

    private void TweenJumping()
    {
        System.Action<ITween<float>> circleRotate = (t) =>
        {
            // start rotation from identity to ensure no stuttering
            selfRigidbody.MoveRotation(Quaternion.Euler(new Vector3(t.CurrentValue, jumpAngle * 180 / Mathf.PI, 0)));
        };

        float startAngle = transform.rotation.eulerAngles.z;
        float endAngle = startAngle + 360.0f;

        // completion defaults to null if not passed in
        gameObject.Tween("RotateCircle", startAngle, endAngle, 0.5f, TweenScaleFunctions.QuadraticEaseOut, circleRotate);
    }

    private void TweenLanding()
    {
        System.Action<ITween<float>> scaleTween = (t) =>
        {
            // start rotation from identity to ensure no stuttering
            Vector3 s = new Vector3(transform.localScale.x, t.CurrentValue, transform.localScale.z);
            transform.localScale = s;
        };
        System.Action<ITween<float>> scaleComplete = (t) =>
        {
            Debug.Log("Player scale complete");
        };

        float startScale = transform.localScale.y;
        float endScale = startScale - 0.3f;

        // completion defaults to null if not passed in
        gameObject  .Tween("ScalePlayer", startScale, endScale, 0.15f, TweenScaleFunctions.QuadraticEaseOut, scaleTween)
                    .ContinueWith(new FloatTween().Setup(endScale, startScale, 0.15f, TweenScaleFunctions.QuadraticEaseOut, scaleTween, scaleComplete));
    }

    private void _SetNextPlatform(Platform p)
    {
        jumpAngle = p.angle;
        nextPlat = p.gameObject;
    }

    private void _Jump()
    {
        canJump = false;
        Vector3 j = new Vector3(
            jumpForward*Mathf.Sin(jumpAngle)*holdTime/2 , 
            jumpUp*holdTime , 
            jumpForward*Mathf.Cos(jumpAngle)*holdTime/2
        );
        selfRigidbody.velocity = transform.TransformDirection(j);
        holdTime = 1.0f;
        isJumping = true;
    }

    private void _JumpSuccess(Collider col)
    {
        //Successfully jump on the platform
        selfRigidbody.freezeRotation = true;
        selfRigidbody.MoveRotation(Quaternion.Euler(Vector3.zero));
        // EventManager.onJumpSuccess.Invoke(col.gameObject);
        EventManager.onJumpSuccess.Invoke(gameObject);
        isCollide = true;
        isJumping = false;
        TweenLanding();
        score += 1;
        scoreText.text = score .ToString();
        Debug.Log("success");
    }

    private void _JumpFail(Collider col)
    {
        isDied = true;
    }

    private void _OnButtonHHold()
    {
        if (isHolding)
        {
            float scaleY = transform.localScale.y + 2*(startScaleY - 0.3f - transform.localScale.y)*Time.deltaTime;
            transform.localScale = new Vector3(transform.localScale.x, scaleY, transform.localScale.z);
            holdTime = Mathf.Min(5, holdTime + 2.5f*Time.deltaTime);
        }
        else
        {
            if (!isCollide)
            {
                float scaleY = transform.localScale.y + 2*(startScaleY - transform.localScale.y)*Time.deltaTime;
                transform.localScale = new Vector3(transform.localScale.x, scaleY, transform.localScale.z);
            }
        }
    }

    private void _OnButtonDown()
    {
        if(Input.GetMouseButtonDown(0) && !isJumping){
            isHolding = true;
        }
    }

    private void _OnButtonUp()
    {
        if(Input.GetMouseButtonUp(0) && isHolding){
            isHolding = false;
            canJump = true;
            isCollide = false;
            selfRigidbody.freezeRotation = false;
            selfRigidbody.MoveRotation(Quaternion.Euler(new Vector3(0, jumpAngle * 180 / Mathf.PI, 0)));
            TweenJumping();
        }
    }

    void OnCollisionEnter(Collision col)
    {
        if (isDied)
            return;
        if (col.collider.gameObject.tag == "Ground")
        {
            _JumpFail(col.collider);
        }
    }

    void OnTriggerEnter(Collider col)
    {
        if (!isFirst)
        {
            isFirst = true;
            return;
        }
        if (isCollide)
            return;
        if (col.gameObject.tag != "Ground")
        {
            if (col.gameObject == nextPlat)
            {
                _JumpSuccess(col);
            }
            else
            {
                if (col.transform.IsChildOf(nextPlat.transform))
                {
                    _JumpSuccess(col);
                }
                else
                {
                    
                }
            }
        }
        else
        {
            _JumpFail(col);
        }
    }
}