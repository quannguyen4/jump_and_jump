using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Platform
{
    public GameObject gameObject;
    public float angle;
}

public class PlatformPool : ObjectPooler
{
    public static PlatformPool platformPooler;
    private float hardened = 1;

    void Awake() 
    {
        platformPooler = this;
        EventManager.onJumpSuccess.AddListener(_onPlayerJumpOnPlatform);
    }

    public override void PostStart()
    {
        for(int i = 0; i < amountToPool; i++)
        {
            pooledObjects[i].transform.parent = transform;
        }
    }

    private void _SpawnPlatform()
    {
        GameObject platform = platformPooler.GetPooledObject();
        if (platform != null)
        {
            //Active to call Awake function and script first
            platform.SetActive(true);
            //
            platform.transform.parent = transform;
        }
    }

    private GameObject _SpawnPlatform(Vector3 p)
    {
        GameObject platform = platformPooler.GetPooledObject();
        if (platform != null)
        {
            //
            platform.transform.parent = transform;
            platform.transform.position = p;
            platform.SetActive(true);
        }
        return platform;
    }

    private void _onPlayerJumpOnPlatform(GameObject p)
    {
        float[] angles = new float[] {Mathf.PI/4, -Mathf.PI/4};
        float angle = angles[Random.Range(0, angles.Length)];
        float distance = Random.Range(1.5f, 2.5f);
        // Debug.Log();
        //
        Vector3 curPos = p.transform.position;
        Vector3 newPos = new Vector3(curPos.x + distance*Mathf.Sin(angle), 0, curPos.z + distance*Mathf.Cos(angle));
        GameObject g = _SpawnPlatform(newPos);
        g.transform.localScale = new Vector3(
            Mathf.Max(0.75f, g.transform.localScale.x*(1.0f/hardened)),
            g.transform.localScale.y,
            Mathf.Max(0.75f, g.transform.localScale.z*(1.0f/hardened))
        );
        g.GetComponent<PlatformMechanic>().TweenIn();
        Platform platform = new Platform();
        platform.gameObject = g;
        platform.angle = angle;
        hardened += 0.05f;
        //
        EventManager.onPlatformSpawned.Invoke(platform);
    }
}
