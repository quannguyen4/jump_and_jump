using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.InteropServices;
using Newtonsoft.Json;

public class Preload : MonoBehaviour
{
    [DllImport("__Internal")]
    private static extern void OnEvent();

    string base64;
    // Start is called before the first frame update
    void Start()
    {
        // dynamic d = JsonConvert.DeserializeObject("{ 'Name': 'Jon Smith', 'Address': { 'City': 'New York', 'State': 'NY' }, 'Age': 42 }");
        // Debug.Log(d.Name);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetStringBase(string s)
    {
        base64 = s;
        Debug.Log(s);
    }

    public void CallEventTest()
    {
        OnEvent();
    }
}
