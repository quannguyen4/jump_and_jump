using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMechanic : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField] private GameObject target;
    private Vector3 baseTargetPos;
    private Vector3 baseCameraPos;
    void Start()
    {
        baseTargetPos = target.transform.position;
        baseCameraPos = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 d = target.transform.position - baseTargetPos;
        transform.position = transform.position + 2*(baseCameraPos + d - transform.position)*Time.deltaTime;
    }
}
