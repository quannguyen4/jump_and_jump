using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPooler : MonoBehaviour
{
    [SerializeField] protected List<GameObject> pooledObjects;
    [SerializeField] protected GameObject[] objectsToPool;
    [SerializeField] protected int amountToPool;

    protected void Start()
    {
        pooledObjects = new List<GameObject>();
        GameObject tmp;
        for(int i = 0; i < amountToPool; i++)
        {
            int type = Random.Range(0, objectsToPool.Length);
            tmp = Instantiate(objectsToPool[type]);
            tmp.SetActive(false);
            pooledObjects.Add(tmp);
        }
        PostStart();
    }

    void Update()
    {
        // Debug.Log("Update");
        _UpdateSpawnObject();
    }

    public GameObject GetPooledObject()
    {
        for(int i = 0; i < amountToPool; i++)
        {
            if(!pooledObjects[i].activeInHierarchy)
            {
                return pooledObjects[i];
            }
        }
        return null;
    }

    public virtual void _UpdateSpawnObject(){}
    public virtual void PostStart(){}
}
