using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public static class EventManager
{
    public static UnityEvent<GameObject> onJumpSuccess      = new UnityEvent<GameObject>();
    public static UnityEvent<Platform> onPlatformSpawned  = new UnityEvent<Platform>();
}
