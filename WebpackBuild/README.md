# pixi_game_template

- To debug: **npm run start**

- To build: **npm run build**

# Config ads network
- Modify **network.js** to change build ads

# Modify ads wrapper
- Go to **src/wrapper** to change source of wrapper

# Template support events
- **gameResume**: resume game
- **gamePause**: pause game
- **gameEnd**: when the game ended
- **onCTACLick**: when user click CTA