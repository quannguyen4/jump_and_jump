module.exports = 
{
    // entry: './src/index.js',
    // entry: inputFilesPath[`${adsTest}`],
    externals:{
        "fs": "commonjs fs"
    },
    module: {
        rules: [
            {
                test: /\.css$/,
                use: [
                    'style-loader',
                    'css-loader',
                ],
            },
            //Image file reader webpack
            {
                test: /\.(png|svg|jpg|gif|jfif)$/,
                use: [
                    {
                        options: {
                            // outputPath: "assets/"
                        },
                        loader: "url-loader"
                    }
                ],
            },
            {
                test: /\.(bin|gltf)$/,
                use: [
                    {
                        options: {
                            // outputPath: "assets/"
                        },
                        loader: "url-loader"
                    }
                ],
            },
            //sound file reader webpack
            {
                test: /\.(mid|mp3|wav)$/,
                use: [
                    {
                        options: {
                            // outputPath: "assets/"
                        },
                        loader: "url-loader"
                    }
                ],
            },
            //font file reader webpack
            {
                test: /\.(woff|woff2|eot|ttf|otf)$/,
                use: [
                    {
                        loader: "url-loader",
                        options: {
                            outputPath: "fonts/"
                        },
                    }
                ],
            },
        ],
    },
};