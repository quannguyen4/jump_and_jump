import _ from 'lodash';
import './style.css';
import MaxApi from "@momo-platform/max-api";
// import * as MaxApiUtils from './MaxApiUtils';
//
if (process.env.NODE_ENV !== 'production') 
{
    console.warn('Looks like we are in development mode!');
}
else
{
    console.warn("Production mode, console cleared");
    console.log = function(){};
}
//

const styles = {
    canvasStyle : {
        // position: 'absolute'
    },
    containerStyle : {
        // textAlign: 'center'
    },
    gameDivStyle : {
        // position: 'relative'
        alignItems: 'center',
        justifyContent: 'center',
        /* align-self: center; */
        height: '100vh',
        display: 'flex',
    }
}

class Index
{
    constructor()
    {

    }
    
    MainGameClosure()
    {
        MaxApi.init({
            "appId": "vn.momo.web.momoacademy",
            "name": "vn.momo.web.momoacademy",
            "displayName": "MoMo Academy",
            "client": {
            "web": {
                "hostId": "vn.momo.web.momoacademy",
                "accessToken": "U2FsdGVkX194ZqKMPmzXS+UkePODhT0lCbCr6KttGeGWgmzKH6Y/QI4g2GeaQbKh+Qz3rW6MO5Z7GLxBKlYBPogGDZ2/QRQWueWL5AdGbOM="
            }
            },
            "configuration_version": 1
        });
        (function(){
            window.addEventListener('unityInited', function(data){
                // console.log(data.detail.unityInstance);
                // MaxApiUtils.getAvatar();
                // data.detail.unityInstance.SendMessage('Loader', 'CallEventTest');
            });
            window.addEventListener('OnEventTest', function(){
                console.log('test received');
            });
        }());
    }

    OnReady()
    {
        this.MainGameClosure();
    }
}

export default Index;