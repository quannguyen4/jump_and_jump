import MaxApi from "@momo-platform/max-api";

class MaxApiUtils
{
    constructor()
    {
        
    }

    getAvatar(){
        return new Promise((res, err) => {
            MaxApi.getContactAvatar(window.gameData.userId,(avatarUrl) => {
                console.log("avatarUrl: ", avatarUrl);
                res(avatarUrl);
            });
        })
    }
}

export default MaxApiUtils;