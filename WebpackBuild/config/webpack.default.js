const path = require('path');
// const { CleanWebpackPlugin } = require('clean-webpack-plugin');

module.exports = {
    entry: './src/wrapper/default.js',
    output: {
        filename: 'bundle.js',
        path: path.resolve(__dirname, '../dist/default'),
    },
    plugins: [
        // new CleanWebpackPlugin(),
    ],
};